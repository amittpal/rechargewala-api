﻿
using AmitCommonLibrary.Models;

namespace RechargeWala_API.AppFunctions.Interfaces
{
    public interface IJWTAuth
    {
        public string Authentication(UserLogin _userLogin);
    }
}