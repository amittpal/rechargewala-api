﻿using AmitCommonLibrary.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using RechargeWala_API.AppFunctions.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace RechargeWala_API.AppFunctions
{
    public class JWTAuth : IJWTAuth
    {
        private IConfiguration _configuration;
        private readonly string username = "amit";
        private readonly string password = "amit";

        public JWTAuth(IConfiguration _config)
        {
            this._configuration = _config;
        }
        public string Authentication(UserLogin _userLogin)
        {

            if (!(username.Equals(this.username) || password.Equals(this.password)))
            {
                return null;
            }
            var roles = new string[] { "Admin", "User" };
            return GenerateJwtToken(_userLogin.UserName, roles.ToList());
        }


        private string GenerateJwtToken(string username, List<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Name, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, username)
            };


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Jwt:ExpiryDays"]));

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}