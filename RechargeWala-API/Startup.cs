using AmitCommonLibrary.Globals;
using AmitCommonLibrary.Models;
using AmitCommonLibrary.Models.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RechargeWala_API.AppFunctions;
using RechargeWala_API.AppFunctions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechargeWala_API
{
    public class Startup
    {
        public Startup(IWebHostEnvironment __env)
        {
            _currentEnvironment = __env;
            var _builder = new ConfigurationBuilder()
                .SetBasePath(__env.ContentRootPath)
                .AddJsonFile(Constants.Api.AppSettingsJSON, optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = _builder.Build();
        }
        private IWebHostEnvironment _currentEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        private void AddMvcCore(MvcOptions options)
        {
            options.RequireHttpsPermanent = true; // this does not affect api requests
            options.RespectBrowserAcceptHeader = true; // false by default
            var _policy = new AuthorizationPolicyBuilder()
                            .RequireAuthenticatedUser()
                            .Build();
            options.Filters.Add(new AuthorizeFilter(_policy));
        }
        private void AddCors(CorsOptions options)
        {
            options.AddPolicy(Constants.Api.CorsPolicy,
               builder =>
               builder.AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader());
        }
        public void AddAuthentication(IServiceCollection services)
        {
        }
        public void AddDependencies(IServiceCollection services)
        {
            services.AddSingleton<IJWTAuth, JWTAuth>();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                AddCors(options);
            });
            services.AddMvc(option => option.EnableEndpointRouting = false);
            services.AddLogging(config =>
            {
                // clear out default configuration
                config.ClearProviders();
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RechargeWala_API", Version = "v1" });
            });

            //Add Cors
            services.AddCors();

          


            AddAuthentication(services);
            AddDependencies(services);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RechargeWala_API v1"));
            }
            app.UseAuthentication();
            app.UseMvc();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
