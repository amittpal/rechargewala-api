﻿using AmitCommonLibrary.CommonFunctions;
using AmitCommonLibrary.Models;
using AmitCommonLibrary.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RechargeWala_API.AppFunctions.Interfaces;
using System;
using System.Collections.Generic;

namespace RechargeWala_API.Controllers
{
    [ApiController, Authorize, Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly ILogger<TokenController> _logger;
        private IFunctionReturn _functionReturn;
        private readonly IJWTAuth _iJWTAuth;

        public TokenController(IJWTAuth __iJWTAuth, ILogger<TokenController> _logger)
        {
            _iJWTAuth = __iJWTAuth;
            this._logger = _logger;
        }


        [AllowAnonymous, HttpPost("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Authentication([FromBody] UserLogin _userLogin)
        {
            //Input check
            string _error = CommonFunction.ValidateUserLoginInput(_userLogin);
            if (!string.IsNullOrEmpty(_error))
            {
                _functionReturn.Message.Add(_error);
            }

            var token = _iJWTAuth.Authentication(_userLogin);
            if (token == null)
                return Unauthorized();
            return Ok(token);
        }

        [Authorize, HttpGet]

        public IActionResult Get()
        {
            return Ok(new List<string>() { "amit", "rohit" });

        }

    }
}